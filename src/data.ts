import {Genre} from "./types/genre.ts";
import {Actor} from "./types/actor.ts";
import {Movie} from "./types/movie.ts";
import {Comment} from "./types/comment.ts";

export const genres: Genre[] = [
    {id: 1, name: "Фантастика", nameSlug: "fantastika"},
    {id: 2, name: "Боевик", nameSlug: "boevik"},
    {id: 3, name: "Драма", nameSlug: "drama"},
    {id: 4, name: "Комедия", nameSlug: "komediya"},
    {id: 5, name: "Ужасы", nameSlug: "uzhasy"},
    {id: 6, name: "Приключения", nameSlug: "priklyucheniya"},
];

export const actors: Actor[] = [
    {id: 1, firstName: "Марк", lastName: "Хэмилл"},
    {id: 2, firstName: "Харрисон", lastName: "Форд"},
    {id: 3, firstName: "Кэрри", lastName: "Фишер"},
    {id: 4, firstName: "Кристиан", lastName: "Бэйл"},
    {id: 5, firstName: "Хит", lastName: "Леджер"},
    {id: 6, firstName: "Аарон", lastName: "Экхарт"},
    {id: 7, firstName: "Натали", lastName: "Портман"},
    {id: 8, firstName: "Леонардо", lastName: "ДиКаприо"},
    {id: 9, firstName: "Дженнифер", lastName: "Лоуренс"},
    {id: 10, firstName: "Том", lastName: "Хэнкс"},
];

export const movies: Movie[] = [
    {
        id: 1,
        title: "Звездные войны: Эпизод IV – Новая надежда",
        titleSlug: "zvezdnye-vojny-epizod-iv-novaja-nadezhda",
        imageName: "zvezdnye-vojny-epizod-iv-novaja-nadezhda.jpg",
        description: "Татуин. Планета-пустыня. Уже постаревший рыцарь Джедай Оби Ван Кеноби спасает молодого Люка Скайуокера, когда тот пытается отыскать пропавшего дроида. С этого момента Люк осознает свое истинное назначение: он один из рыцарей Джедай. В то время как гражданская война охватила галактику, а войска повстанцев ведут бои против сил злого Императора, к Люку и Оби Вану присоединяется отчаянный пилот-наемник Хан Соло, и в сопровождении двух дроидов, R2D2 и C-3PO, этот необычный отряд отправляется на поиски предводителя повстанцев — принцессы Леи. Героям предстоит отчаянная схватка с устрашающим Дартом Вейдером — правой рукой Императора и его секретным оружием — «Звездой Смерти».",
        productionYear: 1977,
        country: "США",
        genres: [genres[0], genres[1]],
        slogan: "Последние джедаи",
        scenario: "Джордж Лукас",
        producer: "Гэри Курц",
        operator: "Гилберт Тейлор",
        budget: 11000000,
        fees: 775398007,
        actors: [actors[0], actors[1], actors[2]],
    }, {
        id: 2,
        title: "Темный рыцарь",
        titleSlug: "temnyj-rycar",
        imageName: "temnyj-rycar.jpg",
        description: "Бэтмен поднимает ставки в войне с криминалом. С помощью лейтенанта Джима Гордона и прокурора Харви Дента он намерен очистить улицы от преступности, отравляющей город. Сотрудничество оказывается эффективным, но скоро они обнаружат себя посреди хаоса, развязанного восходящим криминальным гением, известным испуганным горожанам под именем Джокер.",
        productionYear: 2008,
        country: "США",
        genres: [genres[1], genres[2], genres[5]],
        slogan: "Когда твоя городская среда требует героя",
        scenario: "Кристофер Нолан, Джонатан Нолан",
        producer: "Кристофер Нолан, Эмма Томас",
        operator: "Уолли Пфистер",
        budget: 185000000,
        fees: 1007257536,
        actors: [actors[3], actors[4], actors[5]],
    },
    {
        id: 3,
        title: "Матрица",
        titleSlug: "matrica",
        imageName: "matrica.jpg",
        description: "Мир Матрицы — это иллюзия, существующая только в бесконечном сне обреченного человечества. Холодный мир будущего, в котором люди — всего лишь батарейки в компьютерных системах.",
        productionYear: 1999,
        country: "США",
        genres: [genres[1], genres[5], genres[3]],
        slogan: "Спроси себя: что такое Матрица?",
        scenario: "Лана и Лилли Вачовски",
        producer: "Джоэл Сильвер, Лана и Лилли Вачовски",
        operator: "Билл Поуп",
        budget: 63000000,
        fees: 463500000,
        actors: [actors[8], actors[7], actors[6]],
    },
    {
        id: 4,
        title: "Титаник",
        titleSlug: "titanik",
        imageName: "titanik.jpg",
        description: "В первом и последнем плавании шикарного «Титаника» встречаются двое. Пассажир нижней палубы Джек выиграл билет в карты, а богатая наследница Роза отправляется в Америку, чтобы выйти замуж по расчёту. Чувства молодых людей только успевают расцвести, и даже не классовые различия создадут испытания влюблённым, а айсберг, вставший на пути считавшегося непотопляемым лайнера.",
        productionYear: 1997,
        country: "США",
        genres: [genres[3], genres[5], genres[0]],
        slogan: "Никогда не забуду тебя",
        scenario: "Джеймс Кэмерон",
        producer: "Джеймс Кэмерон, Джон Лэндо, Рейд Моран",
        operator: "Рассел Карпентер",
        budget: 200000000,
        fees: 2208200000,
        actors: [actors[8], actors[7], actors[9]],
    },
    {
        id: 5,
        title: "Пираты Карибского моря: Проклятие Черной жемчужины",
        titleSlug: "piraty-karibskogo-morya-proklyatie-chernoj-zhemchuzhiny",
        imageName: "piraty-karibskogo-morya-proklyatie-chernoj-zhemchuzhiny.jpeg",
        description: "Жизнь харизматичного авантюриста, капитана Джека Воробья, полная увлекательных приключений, резко меняется, когда его заклятый враг — капитан Барбосса — похищает корабль Джека, Черную Жемчужину, а затем нападает на Порт Ройал и крадет прекрасную дочь губернатора, Элизабет Свонн.",
        productionYear: 2003,
        country: "США",
        genres: [genres[2], genres[4], genres[1]],
        slogan: "Аррр, пираты!",
        scenario: "Тед Эллиотт, Терри Россио",
        producer: "Джерри Брукхаймер",
        operator: "Дариусз Вольски",
        budget: 140000000,
        fees: 654300000,
        actors: [actors[9], actors[7], actors[6]],
    },
    {
        id: 6,
        title: "Гладиатор",
        titleSlug: "gladiator",
        imageName: "gladiator.jpg",
        description: "В великой Римской империи не было военачальника, равного генералу Максимусу. Непобедимые легионы, которыми командовал этот благородный воин, боготворили его и могли последовать за ним даже в ад.",
        productionYear: 2000,
        country: "США, Великобритания",
        genres: [genres[1], genres[3], genres[4]],
        slogan: "Смерть или слава",
        scenario: "Дэвид Франкел, Ридли Скотт",
        producer: "Дуглас Уик, Дэвид Франкел, Брайан Грейзер",
        operator: "Джон Мэтисон",
        budget: 103000000,
        fees: 460500000,
        actors: [actors[4], actors[8], actors[9]],
    },
];

export const comments: Comment[] = [
    {id: 1, movieId: 1, name: "Иван", rate: 2, text: "Отличный фильм!"},
    {id: 2, movieId: 1, name: "Дмитрий", rate: 4, text: "Хорошая игра актеров."},
    {id: 3, movieId: 2, name: "Игорь", rate: 5, text: "Эпичный фильм!"},
    {id: 4, movieId: 2, name: "Василий", rate: 1, text: "Замечательная игра актеров."},
    {id: 5, movieId: 3, name: "Михаил", rate: 5, text: "Фильм с шикарным сюжетом!"},
    {id: 6, movieId: 3, name: "Анна", rate: 5, text: "Зрелищные спецэффекты."},
    {id: 7, movieId: 4, name: "София", rate: 5, text: "Этот фильм коснулся моей души!"},
    {id: 8, movieId: 4, name: "Алексей", rate: 4, text: "Замечательная романтическая история."},
    {id: 9, movieId: 5, name: "Антон", rate: 4, text: "Лучшая приключенческая франшиза!"},
    {id: 10, movieId: 5, name: "Виктория", rate: 4, text: "Джонни Депп на высоте."},
    {id: 11, movieId: 6, name: "Владимир", rate: 5, text: "Отличный фильм, великолепная игра Рассела Кроу!"},
    {id: 12, movieId: 6, name: "Марина", rate: 4, text: "Зрелищные боевые сцены."},
];
