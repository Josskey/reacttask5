import {Outlet} from "react-router-dom";
import Header from "../components/page/header.tsx";
import {useMoviesStore} from "../store/movies.ts";
import {useMovies} from "../hooks/movies/use-movies.ts";
import SidebarBlock from "../components/page/sidebar/sidebar-block.tsx";
import SidebarBlockList from "../components/page/sidebar/sidebar-block-list.tsx";
import {SidebarListElement} from "../types/ui/sidebar/sidebar-list-element.ts";

export default function Root() {
    const {findMovieById} = useMovies();
    const {favoriteIds, watchLaterIds} = useMoviesStore();

    return <>
        <Header/>
        <main className="mx-auto max-w-7xl mt-4 mb-20 flex gap-4">
            <section className="flex-1">
                <Outlet/>
            </section>
            <aside className="w-64 space-y-4">
                <SidebarBlock title="Избранное">
                    <SidebarBlockList elements={favoriteIds.map(favoriteId => {
                        const movie = findMovieById(favoriteId);
                        return movie !== undefined ? {
                            key: movie.id.toString(),
                            label: movie.title,
                            to: `/movie/${movie.titleSlug}`,
                        } : undefined;
                    }).filter(element => element !== undefined) as SidebarListElement[]}
                                      emptyError="Вы пока ничего не добавили в избранное."/>
                </SidebarBlock>
                <SidebarBlock title="Смотреть позже">
                    <SidebarBlockList elements={watchLaterIds.map(watchLaterId => {
                        const movie = findMovieById(watchLaterId);
                        return movie !== undefined ? {
                            key: movie.id.toString(),
                            label: movie.title,
                            to: `/movie/${movie.titleSlug}`,
                        } : undefined;
                    }).filter(element => element !== undefined) as SidebarListElement[]}
                                      emptyError="Вы пока ничего не добавили в этот список."/>
                </SidebarBlock>
            </aside>
        </main>
    </>;
}
