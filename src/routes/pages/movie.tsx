import {useParams} from "react-router-dom";
import {movies} from "../../data.ts";
import ErrorBox from "../../components/page/error-box.tsx";
import SimilarMovies from "../../components/movie/similar-movies.tsx";
import MovieFullInfo from "../../components/movie/movie-full-info.tsx";
import MovieComments from "../../components/movie/movie-comments.tsx";
import {MovieContextProvider} from "../../providers/movies/movie-context-provider.tsx";

export default function MoviePage() {
    const {slug} = useParams();
    const movie = movies.find(movie => movie.titleSlug === slug);

    if (movie === undefined) {
        return <ErrorBox className="mt-4">
            К сожалению, фильм не найден.
        </ErrorBox>;
    }

    return <MovieContextProvider movie={movie}>
        <MovieFullInfo/>
        <section className="mt-4">
            <SimilarMovies/>
        </section>
        <section className="mt-4">
            <MovieComments/>
        </section>
    </MovieContextProvider>;
}
