import {useSearchQuery} from "../../hooks/search/use-search-query.ts";
import GenresSelect from "../../components/genre/genres-select.tsx";
import MoviesGrid from "../../components/movie/movies-grid.tsx";
import {useGenresFromSearchParams} from "../../hooks/genres/use-genres-from-search-params.ts";
import {useMovies} from "../../hooks/movies/use-movies.ts";
import {useGenres} from "../../hooks/movies/use-genres.ts";
import MovieCard from "../../components/movie/movie-card.tsx";
import ErrorBox from "../../components/page/error-box.tsx";

export const GENRES_PARAM_KEY = 'genres';

export default function SearchPage() {
    const {searchQuery} = useSearchQuery();
    const {genres} = useGenres();
    const {selected: selectedGenres} = useGenresFromSearchParams(GENRES_PARAM_KEY);
    const {movies} = useMovies(
        'desc',
        genres.filter(genre => selectedGenres.includes(genre.nameSlug)),
        searchQuery,
    );

    return <>
        <section className="flex justify-between items-center">
            <GenresSelect searchParamKey={GENRES_PARAM_KEY}/>
        </section>
        {movies.length > 0 && <MoviesGrid className="mt-4">
            {movies.map((movie) => {
                return <MovieCard key={movie.id}
                                  movie={movie}/>;
            })}
        </MoviesGrid>}
        {movies.length === 0 &&
            <ErrorBox className="mt-4">
                К сожалению, фильмов по Вашему запросу не найдено.
            </ErrorBox>}
    </>;
}
