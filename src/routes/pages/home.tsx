import {useMovies} from "../../hooks/movies/use-movies.ts";
import MovieCard from "../../components/movie/movie-card.tsx";
import MoviesGrid from "../../components/movie/movies-grid.tsx";
import {useSearchParams} from "react-router-dom";
import SearchParamsSelect from "../../components/forms/search-params-select.tsx";
import GenresSelect from "../../components/genre/genres-select.tsx";
import {useGenresFromSearchParams} from "../../hooks/genres/use-genres-from-search-params.ts";
import {useGenres} from "../../hooks/movies/use-genres.ts";

const RATE_PARAM_KEY = 'rate';
const GENRES_PARAM_KEY = 'genres';

export default function HomePage() {
    const [searchParams] = useSearchParams();
    const {genres} = useGenres();
    const {selected: selectedGenres} = useGenresFromSearchParams(GENRES_PARAM_KEY);
    const {
        movies,
    } = useMovies(searchParams.get(RATE_PARAM_KEY) ?? 'desc', genres.filter(genre => selectedGenres.includes(genre.nameSlug)));

    return <>
        <section className="flex justify-between items-center">
            <GenresSelect searchParamKey={GENRES_PARAM_KEY}/>
            <SearchParamsSelect searchParamKey={RATE_PARAM_KEY}
                                label="Сортировка"
                                options={[
                                    {value: 'desc', label: 'по рейтингу (сначала высокие)'},
                                    {value: 'asc', label: 'по рейтингу (сначала низкие)'},
                                ]}/>
        </section>
        <MoviesGrid className="mt-4">
            {movies.map((movie) => {
                return <MovieCard key={movie.id}
                                  movie={movie}/>;
            })}
        </MoviesGrid>
    </>;
}
