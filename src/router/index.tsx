import {createBrowserRouter} from "react-router-dom";
import HomePage from "../routes/pages/home.tsx";
import ErrorPage from "../routes/error-page.tsx";
import Root from "../routes/root.tsx";
import SearchPage from "../routes/pages/search.tsx";
import MoviePage from "../routes/pages/movie.tsx";

export const routes = createBrowserRouter([
    {
        path: '/',
        element: <Root/>,
        errorElement: <ErrorPage/>,
        children: [
            {
                path: '/',
                element: <HomePage/>,
            }, {
                path: '/search',
                element: <SearchPage/>,
            }, {
                path: '/movie/:slug',
                element: <MoviePage/>,
            },
        ],
    },
]);
