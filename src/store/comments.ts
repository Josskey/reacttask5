import {create} from "zustand";
import {persist} from "zustand/middleware";
import {Comment} from "../types/comment.ts";
import {comments} from "../data.ts";


type CommentsStoreState = {
    comments: Comment[];
    addComment: (movieId: number, name: string, rate: number, text: string) => void;
};

export const useCommentsStore = create(persist<CommentsStoreState>(
    (setState) => ({
        comments: comments,
        addComment: (movieId: number, name: string, rate: number, text: string) => {
            setState(prev => ({
                comments: [...prev.comments, {
                    id: prev.comments[prev.comments.length - 1].id + 1,
                    movieId,
                    name,
                    rate,
                    text,
                }]
            }));
        },
    }),
    {
        name: 'comments-store',
    }
));
