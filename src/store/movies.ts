import {create} from "zustand";
import {persist} from "zustand/middleware";


type MoviesStoreState = {
    favoriteIds: number[];
    watchLaterIds: number[];
    toggleFavorite: (id: number) => void;
    toggleWatchLater: (id: number) => void;
};

export const useMoviesStore = create(persist<MoviesStoreState>(
    (setState) => ({
        favoriteIds: [],
        watchLaterIds: [],
        toggleFavorite: (id: number) => {
            setState(prev => ({
                favoriteIds: !prev.favoriteIds.includes(id)
                    ? [...prev.favoriteIds, id]
                    : prev.favoriteIds.filter(existId => existId !== id),
            }));
        },
        toggleWatchLater: (id: number) => {
            setState(prev => ({
                watchLaterIds: !prev.watchLaterIds.includes(id)
                    ? [...prev.watchLaterIds, id]
                    : prev.watchLaterIds.filter(existId => existId !== id),
            }));
        },
    }),
    {
        name: 'movies-store',
    }
));
