import {ChangeEvent} from "react";
import {useSearchParams} from "react-router-dom";

type SearchParamsSelectProps = {
    searchParamKey: string;
    label: string;
    options: { value: string, label: string }[];
};

export default function SearchParamsSelect(props: SearchParamsSelectProps) {
    const [searchParams, setSearchParams] = useSearchParams();
    const sort = searchParams.get(props.searchParamKey) ?? '';

    const onChange = (e: ChangeEvent<HTMLSelectElement>) => {
        searchParams.set(props.searchParamKey, e.target.value);
        setSearchParams(searchParams);
    }

    return <section className="flex gap-1 text-sm">
        <span className="text-gray-500">{props.label}:</span>
        <select
            value={sort}
            name={props.searchParamKey}
            onChange={onChange}
            className="appearance-none bg-transparent text-purple-500 font-medium hover:outline-none">
            {props.options.map(option => {
                return <option key={option.value}
                               value={option.value}>{option.label}</option>;
            })}
        </select>
    </section>;
}
