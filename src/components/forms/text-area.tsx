import {ChangeEvent, useCallback} from "react";

type TextAreaProps = {
    value: string;
    onChange: (e: ChangeEvent<HTMLTextAreaElement>) => void;
    required?: boolean;
    error?: string;
};

export default function TextArea(props: TextAreaProps) {
    const onChange = useCallback(
        (e: ChangeEvent<HTMLTextAreaElement>) => {
            props.onChange(e);
        },
        [props],
    );

    return <section>
            <textarea value={props.value}
                      name="text"
                      rows={3}
                      required={props.required}
                      className="border-2 rounded px-4 py-2 outline-none w-full"
                      onChange={onChange}/>
        {props.error && <div className="text-red-500 font-medium text-sm">
            {props.error}
        </div>}
    </section>
}