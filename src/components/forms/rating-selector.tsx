import {StarIcon} from "@heroicons/react/24/solid";
import {ChangeEvent, useCallback} from "react";

type RatingSelectorProps = {
    value: string;
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
    error?: string;
};

export default function RatingSelector(props: RatingSelectorProps) {
    const onChange = useCallback(
        (e: ChangeEvent<HTMLInputElement>) => {
            props.onChange(e);
        },
        [props],
    );

    return <div className="flex items-center">
        {[...Array(5)].map((_, index) => {
            const rating = index + 1;

            return <label key={rating}>
                <input type="radio"
                       className="hidden"
                       value={rating} onChange={onChange} name="rating"/>
                <StarIcon
                    className={`size-5 ${rating <= parseInt(props.value) ? 'text-purple-500' : 'text-gray-500'}`}/>
            </label>;
        })}
        {props.error && <div className="ml-3 text-red-500 font-medium text-sm">
            {props.error}
        </div>}
    </div>;
}