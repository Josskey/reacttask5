import {useMovieComments} from "../../hooks/movies/use-comments.ts";
import ErrorBox from "../page/error-box.tsx";
import MovieCommentForm from "./movie-comments/movie-comment-form.tsx";
import MovieComment from "./movie-comments/movie-comment.tsx";
import {useMovieContext} from "../../providers/movies/movie-context.ts";

export default function MovieComments() {
    const movie = useMovieContext();
    const {comments} = useMovieComments(movie.id);

    return <>
        <h2 className="text-2xl text-transparent bg-clip-text bg-gradient-to-r from-red-400 to-purple-500 w-max font-medium ml-4">Комментарии:</h2>
        <section className="mt-3 space-y-2">
            {comments.map((comment) => {
                return <MovieComment key={comment.id}
                                     comment={comment}/>;
            })}
            {comments.length === 0 && <ErrorBox>
                Никто пока не оставлял комментариев к этому фильму.
            </ErrorBox>}
        </section>
        <section className="mt-4">
            <MovieCommentForm movieId={movie.id}/>
        </section>
    </>;
}