import MoviesGrid from "./movies-grid.tsx";
import MovieCard from "./movie-card.tsx";
import {useSimilarMovies} from "../../hooks/movies/use-similar-movies.ts";
import {useMovieContext} from "../../providers/movies/movie-context.ts";

export default function SimilarMovies() {
    const movie = useMovieContext();
    const similarMovies = useSimilarMovies(movie);

    if (similarMovies.length === 0) {
        return null;
    }
    return <>
        <h2 className="text-2xl text-transparent bg-clip-text bg-gradient-to-r from-red-400 to-purple-500 w-max font-medium ml-4">Похожее:</h2>
        <MoviesGrid className="mt-3">
            {similarMovies.map((movie) => {
                return <MovieCard key={movie.id}
                                  movie={movie}/>;
            })}
        </MoviesGrid>
    </>;
}