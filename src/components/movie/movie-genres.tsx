import {Genre} from "../../types/genre.ts";
import GenreLink from "../genre/genre-link.tsx";

export default function MovieGenres({genres}: { genres: Genre[] }) {
    return <section>
        {genres.map((genre, i) => {
            return <GenreLink genreSlug={genre.nameSlug}
                              key={genre.id}>
                {genre.name}{(genres.length - 1) == i ? '' : ', '}
            </GenreLink>;
        })}
    </section>;
}