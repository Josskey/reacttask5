import Rating from "./rating.tsx";
import FavoritesManage from "./favorites-manage.tsx";
import MovieDescriptionLine from "./movie-description-line.tsx";
import MovieGenres from "./movie-genres.tsx";
import {formatMillions} from "../../functions.ts";
import MovieActors from "./movie-actors.tsx";
import {useMovieContext} from "../../providers/movies/movie-context.ts";

export default function MovieFullInfo() {
    const movie = useMovieContext();

    return <section className="relative grid grid-cols-6 gap-4">
        <img src={`/images/movies/${movie.imageName}`}
             alt={movie.title}
             className="col-span-2 w-full h-96 rounded object-cover"/>
        <div className="absolute top-2.5 left-2.5 flex items-start gap-2">
            <Rating movieId={movie.id}/>
            <FavoritesManage movieId={movie.id}/>
        </div>
        <section className="col-span-4">
            <h1 className="text-5xl text-center text-transparent bg-clip-text bg-gradient-to-r from-red-400 to-purple-500 font-extrabold">
                {movie.title}
            </h1>
            <p className="text-white bg-gradient-to-r from-red-400 to-purple-500 font-medium rounded px-4 py-2 mt-4">
                {movie.description}
            </p>
            <section className="bg-white rounded mt-4 px-4 py-2 space-y-1">
                <MovieDescriptionLine label="Год производства">
                    {movie.productionYear}
                </MovieDescriptionLine>
                <MovieDescriptionLine label="Страна">
                    {movie.country}
                </MovieDescriptionLine>
                <MovieDescriptionLine label="Жанры">
                    <MovieGenres genres={movie.genres}/>
                </MovieDescriptionLine>
                <MovieDescriptionLine label="Слоган">
                    {movie.slogan}
                </MovieDescriptionLine>
                <MovieDescriptionLine label="Сценарий">
                    {movie.scenario}
                </MovieDescriptionLine>
                <MovieDescriptionLine label="Продюсер">
                    {movie.producer}
                </MovieDescriptionLine>
                <MovieDescriptionLine label="Оператор">
                    {movie.operator}
                </MovieDescriptionLine>
                <MovieDescriptionLine label="Бюджет">
                    {formatMillions(movie.budget)}
                </MovieDescriptionLine>
                <MovieDescriptionLine label="Сборы в мире">
                    {formatMillions(movie.fees)}
                </MovieDescriptionLine>
                <MovieDescriptionLine label="Актёры">
                    <MovieActors actors={movie.actors}/>
                </MovieDescriptionLine>
            </section>
        </section>
    </section>;
}
