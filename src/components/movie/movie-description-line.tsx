import {ReactNode} from "react";

export default function MovieDescriptionLine({label, children}: { label: string, children: ReactNode }) {
    return <section className="grid grid-cols-4">
        <span className="text-gray-500">{label}</span>
        <span className="col-span-3 font-medium">{children}</span>
    </section>;
}
