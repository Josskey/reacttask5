import {useCommentForm} from "../../../hooks/movies/use-comment-form.ts";
import TextArea from "../../forms/text-area.tsx";
import SubmitButton from "../../buttons/submit-button.tsx";
import RatingSelector from "../../forms/rating-selector.tsx";

export default function MovieCommentForm({movieId}: { movieId: number }) {
    const {
        data,
        errors,
        handleChange,
        handleSubmit,
    } = useCommentForm(movieId);

    return <form onSubmit={handleSubmit}
                 className="bg-white rounded p-4">
        <TextArea value={data.text}
                  required={true}
                  error={errors.text}
                  onChange={handleChange}/>
        <section className="mt-2">
            <RatingSelector value={data.rating}
                            onChange={handleChange}
                            error={errors.rating}/>
        </section>
        <SubmitButton type="submit">Отправить</SubmitButton>
    </form>;
}