import {StarIcon} from "@heroicons/react/24/solid";
import {formatNumber} from "../../../functions.ts";
import {Comment} from "../../../types/comment.ts";

export default function MovieComment({comment}: { comment: Comment }) {
    return <div key={comment.id} className="bg-white rounded flex justify-between items-start py-2 px-4">
        <section>
            <h3 className="text-sm text-gray-500"><strong
                className="font-medium text-transparent bg-clip-text bg-gradient-to-r from-red-400 to-purple-500">{comment.name}</strong> прокомментарировал(-а):
            </h3>
            <p>{comment.text}</p>
        </section>
        <div className="flex font-semibold text-purple-400 gap-1 items-center">
            <StarIcon className="size-5"/>
            {formatNumber(comment.rate, 0)}
        </div>
    </div>;
}