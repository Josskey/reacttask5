import {ReactNode} from "react";

export default function MoviesGrid({children, className}: { children: ReactNode, className?: string }) {
    return <section className={`grid grid-cols-3 gap-4 ${className ?? ''}`}>
        {children}
    </section>
}
