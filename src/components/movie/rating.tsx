import {StarIcon} from "@heroicons/react/24/solid";
import {useMovieComments} from "../../hooks/movies/use-comments.ts";
import {formatNumber} from "../../functions.ts";

export default function Rating({movieId, className}: { movieId: number, className?: string }) {
    const {rating} = useMovieComments(movieId);

    return <div className={`flex font-semibold text-purple-400 text-sm gap-1 items-center ${className ?? ''}`}>
        <StarIcon className="size-4"/>
        {formatNumber(rating, 1)}
    </div>;
}