import {Actor} from "../../types/actor.ts";

export default function MovieActors({actors}: { actors: Actor[] }) {
    return <section>
        {actors.map((actor, i) => {
            return <span
                key={actor.id}>{actor.firstName} {actor.lastName}{(actors.length - 1) == i ? '' : ', '}</span>
        })}
    </section>;
}