import {ClockIcon, HeartIcon} from "@heroicons/react/24/solid";
import {useMoviesStore} from "../../store/movies.ts";

export default function FavoritesManage({movieId, className}: { movieId: number, className?: string }) {
    const {
        favoriteIds,
        watchLaterIds,
        toggleFavorite,
        toggleWatchLater,
    } = useMoviesStore();

    const onToggleFavorite = () => {
        toggleFavorite(movieId);
    }

    const onToggleWatchLater = () => {
        toggleWatchLater(movieId);
    }

    return <div className={`flex text-white gap-1 ${className ?? ''}`}>
        <button type="button"
                onClick={onToggleFavorite}>
            <HeartIcon
                className={`size-5 transition hover:scale-125 ${favoriteIds.includes(movieId) ? 'text-purple-400' : ''}`}/>
        </button>
        <button type="button"
                onClick={onToggleWatchLater}>
            <ClockIcon
                className={`size-5 transition hover:scale-125 ${watchLaterIds.includes(movieId) ? 'text-purple-400' : ''}`}/>
        </button>
    </div>;
}