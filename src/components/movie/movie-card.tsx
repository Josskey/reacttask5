import LinkWrapper from "../link/link-wrapper.tsx";
import {Movie} from "../../types/movie.ts";
import {strLimit} from "../../functions.ts";
import FavoritesManage from "./favorites-manage.tsx";
import Rating from "./rating.tsx";
import MovieGenres from "./movie-genres.tsx";
import MovieActors from "./movie-actors.tsx";

export default function MovieCard({movie}: { movie: Movie }) {
    return <div
        className="bg-white relative rounded group h-96 shadow overflow-hidden hover:scale-105 hover:shadow-2xl transition">
        <img src={`/images/movies/${movie.imageName}`}
             alt={movie.title}
             className="h-full absolute w-full object-cover"/>
        <Rating movieId={movie.id}
                className="absolute top-2.5 right-2.5"/>
        <FavoritesManage movieId={movie.id}
                         className="absolute top-2.5 left-2.5"/>
        <div
            className="absolute bottom-0 w-full bg-gradient-to-b flex gap-2 flex-col justify-end from-transparent h-64 to-black text-white p-4">
            <LinkWrapper to={`/movie/${movie.titleSlug}`}
                         className="font-medium leading-4 hover:text-purple-400 transition">{movie.title}</LinkWrapper>
            <div className="flex flex-col gap-0.5 text-xs">
                <MovieGenres genres={movie.genres}/>
                <MovieActors actors={movie.actors}/>
            </div>
            <p className="text-xs text-white/70">{strLimit(movie.description, 200)}</p>
        </div>
    </div>;
}