import {Link} from "react-router-dom";
import {CSSProperties, ReactNode} from "react";

type LinkWrapperProps = {
    to: string;
    className?: string;
    style?: CSSProperties;
    children?: ReactNode;
};

export default function LinkWrapper(props: LinkWrapperProps) {
    return <Link to={props.to}
                 className={props.className}
                 style={props.style}>
        {props.children}
    </Link>
}
