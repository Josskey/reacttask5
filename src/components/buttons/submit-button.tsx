import {ReactNode} from "react";

type SubmitButtonProps = {
    type: 'button' | 'submit';
    children: ReactNode;
};

export default function SubmitButton(props: SubmitButtonProps) {
    return <button type={props.type}
                   className="bg-gradient-to-r from-red-400 to-purple-500 text-white rounded py-3 px-10 mt-4 font-medium">
        {props.children}
    </button>;
}