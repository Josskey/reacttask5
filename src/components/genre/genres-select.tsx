import {useRef, useState} from "react";
import useOutsideClick from "../../hooks/use-outside-click.ts";
import {useSearchParams} from "react-router-dom";
import {useGenresFromSearchParams} from "../../hooks/genres/use-genres-from-search-params.ts";
import {useGenres} from "../../hooks/movies/use-genres.ts";

export default function GenresSelect({searchParamKey}: { searchParamKey: string }) {
    const {genres, findGenreByNameSlug} = useGenres();
    const [searchParams, setSearchParams] = useSearchParams();
    const {selected} = useGenresFromSearchParams(searchParamKey);

    const ref = useRef<HTMLDivElement>(null);
    const [opened, setOpened] = useState(false);

    const toggleOpened = () => {
        setOpened(prev => !prev);
    }

    const toggleSelected = (slug: string) => {
        let temp = selected;
        if (selected.includes(slug)) {
            temp = temp.filter(selectedSlug => selectedSlug !== slug);
        } else {
            temp = [...temp, slug];
        }

        if (temp.length === 0) {
            searchParams.delete(searchParamKey);
        } else {
            searchParams.set(searchParamKey, temp.join(','));
        }

        setSearchParams(searchParams);
    }

    useOutsideClick(() => {
        if (opened) {
            toggleOpened();
        }
    }, ref);

    return <div className="relative w-64 text-sm" ref={ref}>
        <button type="button"
                onClick={toggleOpened}
                className={`bg-white w-full py-2 px-4 font-medium ${opened ? 'rounded-t' : 'rounded'}`}>
            <span>Выбрать категории</span>
            {selected.length > 0 && <div className="text-xs text-gray-500">
                {selected.map((genre, i) => {
                    return <span key={genre}>
                        {findGenreByNameSlug(genre)?.name ?? genre}{(selected.length - 1) === i ? '' : ', '}
                    </span>;
                })}
            </div>}
        </button>
        {opened && <div
            className="absolute z-10 bg-white w-full rounded-b shadow-2xl py-2 overflow-hidden">
            <section className="flex flex-col border-t-2 max-h-64 overflow-x-auto">
                {genres.map(genre => {
                    const isSelected = selected.includes(genre.nameSlug);

                    return <button type="button"
                                   key={genre.nameSlug}
                                   onClick={() => toggleSelected(genre.nameSlug)}
                                   className={`border-b-2 py-1.5 px-3 text-left font-medium ${isSelected ? 'bg-purple-100 text-purple-500 border-purple-200' : 'hover:bg-purple-100 hover:text-purple-500 hover:border-purple-200'}`}>
                        {genre.name}
                    </button>
                })}
            </section>
        </div>}
    </div>;
}