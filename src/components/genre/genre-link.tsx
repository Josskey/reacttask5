import LinkWrapper from "../link/link-wrapper.tsx";
import {ReactNode} from "react";

export default function GenreLink({genreSlug, children}: { genreSlug: string, children: ReactNode }) {
    const queryParams = new URLSearchParams();
    queryParams.set('genres', genreSlug);

    return <LinkWrapper to={`/search?${queryParams.toString()}`}
                        className="hover:text-purple-400 transition">
        {children}
    </LinkWrapper>;
}