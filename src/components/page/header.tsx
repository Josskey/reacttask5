import LinkWrapper from "../link/link-wrapper.tsx";
import Search from "./header/search.tsx";

export default function Header() {
    return <header className="bg-white h-14 shadow">
        <section className="mx-auto max-w-7xl h-full grid items-center grid-cols-12">
            <LinkWrapper to="/"
                         className="col-span-4 h-full w-max flex items-center justify-between pr-4 hover:text-purple-400 transition font-medium">
                Каталог фильмов
            </LinkWrapper>
            <Search/>
        </section>
    </header>;
}