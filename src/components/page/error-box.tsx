import {ReactNode} from "react";

type ErrorBoxProps = {
    className?: string;
    children?: ReactNode;
};

export default function ErrorBox(props: ErrorBoxProps) {
    return <div
        className={`max-w-lg text-center mx-auto bg-gray-200 rounded py-2 px-4 font-medium ${props.className ?? ''}`}>
        {props.children}
    </div>;
}
