import {ChangeEvent, useEffect, useReducer} from "react";
import {useLocation, useNavigate, useSearchParams} from "react-router-dom";
import {useGenresFromSearchParams} from "../../../hooks/genres/use-genres-from-search-params.ts";
import {GENRES_PARAM_KEY} from "../../../routes/pages/search.tsx";

export const SEARCH_PARAM_KEY = 'query';

type State = {
    searchQuery: string;
};

const initialState: State = {
    searchQuery: '',
};

enum ActionTypes {
    SET_SEARCH_QUERY = 'SET_SEARCH_QUERY',
}

interface Action {
    type: ActionTypes;
    payload: string;
}

const reducer = (state: State, action: Action): State => {
    switch (action.type) {
        case ActionTypes.SET_SEARCH_QUERY:
            return {
                ...state,
                searchQuery: action.payload,
            };
        default:
            return state;
    }
};

export default function Search() {
    const location = useLocation();
    const navigate = useNavigate();
    const {selected: genres} = useGenresFromSearchParams(GENRES_PARAM_KEY);
    const [searchParams, setSearchParams] = useSearchParams();
    const pathName = location.pathname;

    const [state, dispatch] = useReducer(reducer, initialState);

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        dispatch({type: ActionTypes.SET_SEARCH_QUERY, payload: value});

        if (value !== '') {
            searchParams.set(SEARCH_PARAM_KEY, value);
        } else {
            searchParams.delete(SEARCH_PARAM_KEY);
        }

        setSearchParams(searchParams);
    };

    useEffect(() => {
        if (state.searchQuery !== '' && pathName !== '/search') {
            navigate(`/search?${SEARCH_PARAM_KEY}=` + state.searchQuery);
        }
        if (state.searchQuery === '' && genres.length === 0 && pathName === '/search') {
            navigate('/');
        }
    }, [state.searchQuery, pathName, navigate, genres.length]);

    return (
        <div className="col-span-4">
            <input
                type="text"
                value={state.searchQuery}
                onChange={handleChange}
                className="h-full appearance-none w-full outline-none border rounded px-3 py-2"
                placeholder="Поиск..."
            />
        </div>
    );
}
