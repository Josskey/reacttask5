import LinkWrapper from "../../link/link-wrapper.tsx";
import {strLimit} from "../../../functions.ts";
import {SidebarListElement} from "../../../types/ui/sidebar/sidebar-list-element.ts";

type SidebarBlockListProps = {
    elements: SidebarListElement[];
    emptyError: string;
};

export default function SidebarBlockList(props: SidebarBlockListProps) {
    if (props.elements.length === 0) {
        return <div className="text-sm text-gray-500">
            {props.emptyError}
        </div>;
    }

    return <section className="flex flex-col gap-1">
        {props.elements.map((element) => {
            return <LinkWrapper key={element.key}
                                className="bg-gradient-to-r from-red-400 to-purple-500 px-3 py-1.5 text-white rounded hover:scale-110 hover:shadow-2xl transition"
                                to={element.to}>
                {strLimit(element.label, 20)}
            </LinkWrapper>;
        })}
    </section>;
}
