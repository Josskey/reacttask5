import {ReactNode} from "react";

export default function SidebarBlock({title, children}: { title: string, children: ReactNode }) {
    return <section className="bg-white rounded p-4">
        <h3 className="font-semibold text-xl text-transparent bg-clip-text bg-gradient-to-r from-red-400 to-purple-500">
            {title}
        </h3>
        <section className="mt-1">
            {children}
        </section>
    </section>;
}