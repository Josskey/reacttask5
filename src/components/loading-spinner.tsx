import './loading-spinner.css';

export default function LoadingSpinner() {
    return <div className="w-full flex justify-center items-center py-3">
        <span className="loader"></span>
    </div>;
}