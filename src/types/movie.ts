import {Actor} from "./actor.ts";
import {Genre} from "./genre.ts";

export type Movie = {
    id: number;
    title: string;
    titleSlug: string;
    description: string;
    imageName: string;
    productionYear: number;
    country: string;
    genres: Genre[];
    slogan: string;
    scenario: string;
    producer: string;
    operator: string;
    budget: number;
    fees: number;
    actors: Actor[];
};
