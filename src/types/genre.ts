export type Genre = {
    id: number;
    name: string;
    nameSlug: string;
};