export type Comment = {
    id: number;
    movieId: number;
    name: string;
    rate: number;
    text: string;
};