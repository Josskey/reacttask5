export type Actor = {
    id: number;
    firstName: string;
    lastName: string;
};