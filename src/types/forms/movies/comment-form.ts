export type CommentForm = {
    text: string;
    rating: string;
};
export type CommentFormErrors = {
    text?: string;
    rating?: string;
};