export type SidebarListElement = {
    key: string;
    label: string;
    to: string;
};
