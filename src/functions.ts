export const strLimit = (str: string, limit: number) => {
    if (str.length > limit) {
        return str.substring(0, limit) + '...';
    }
    return str;
}

export const formatMillions = (amount: number) => {
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 0,
    });

    return `${formatter.format(amount / 1000000)} млн.`;
}

export const formatNumber = (amount: number, digits: number = 2) => {
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'decimal',
        minimumFractionDigits: digits,
        maximumFractionDigits: digits,
    });

    return formatter.format(amount);
}
