import {MutableRefObject, useCallback, useEffect} from "react";

const MOUSE_DOWN = 'mousedown';

export default function useOutsideClick(handleClose: () => void, ref: MutableRefObject<HTMLDivElement | null>) {
    const handleClick = useCallback((event: MouseEvent) => {
        if (ref?.current?.contains && !ref.current.contains(event.target as Node)) {
            handleClose();
        }
    }, [handleClose, ref]);

    useEffect(() => {
        document.addEventListener(MOUSE_DOWN, handleClick);

        return () => {
            document.removeEventListener(MOUSE_DOWN, handleClick);
        };
    }, [handleClick]);
}
