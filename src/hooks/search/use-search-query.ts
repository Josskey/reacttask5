import {useSearchParams} from "react-router-dom";
import {SEARCH_PARAM_KEY} from "../../components/page/header/search.tsx";

export const useSearchQuery = () => {
    const [searchParams] = useSearchParams();

    return {
        searchQuery: searchParams.get(SEARCH_PARAM_KEY) ?? '',
    }
}
