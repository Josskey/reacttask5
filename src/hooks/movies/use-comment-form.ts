import {ChangeEvent, FormEvent, useState} from "react";
import {CommentForm, CommentFormErrors} from "../../types/forms/movies/comment-form.ts";
import {useCommentsStore} from "../../store/comments.ts";

export const useCommentForm = (movieId: number) => {
    const {addComment} = useCommentsStore();
    const [data, setData] = useState<CommentForm>({
        text: '',
        rating: '',
    });
    const [errors, setErrors] = useState<CommentFormErrors>({});

    const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setData(prev => ({...prev, [e.target.name]: e.target.value}));
    }

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        setErrors({});

        if (data.text === '') {
            setErrors({text: 'Пожалуйста, введите текст.'});
            return;
        }

        if (data.rating === '') {
            setErrors({rating: 'Пожалуйста, выберите оценку.'});
            return;
        }

        addComment(movieId, 'Анонимный пользователь', parseInt(data.rating), data.text);
        setData({
            text: '',
            rating: '',
        });
    }

    return {
        data,
        errors,
        handleChange,
        handleSubmit,
    };
}
