import {useCommentsStore} from "../../store/comments.ts";

export const useMovieComments = (movieId: number) => {
    const {comments} = useCommentsStore();
    const movieComments = comments.filter(comment => comment.movieId === movieId);

    return {
        comments: movieComments,
        rating: movieComments.length > 0 ? movieComments.reduce((rating, comment) => rating + comment.rate, 0) / movieComments.length : 0,
    };
}