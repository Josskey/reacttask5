import {Movie} from "../../types/movie.ts";
import {Genre} from "../../types/genre.ts";
import {useMovies} from "./use-movies.ts";

export const useSimilarMovies = (movie: Movie | undefined) => {
    const {movies} = useMovies();
    if (movie === undefined) return [];

    const similarMovies: Movie[] = [];

    const getCommonGenres = (movie1: Movie, movie2: Movie): Genre[] => {
        const commonGenres: Genre[] = [];
        for (const genre1 of movie1.genres) {
            for (const genre2 of movie2.genres) {
                if (genre1.id === genre2.id) {
                    commonGenres.push(genre1);
                    break;
                }
            }
        }
        return commonGenres;
    };

    for (const otherMovie of movies) {
        if (movie.id !== otherMovie.id) {
            const commonGenres = getCommonGenres(movie, otherMovie);
            if (movie.genres.length >= 2 && commonGenres.length >= 2
                || movie.genres.length === 1 && commonGenres.length === 1) {
                similarMovies.push(otherMovie);
            }
        }
    }

    return similarMovies;
}