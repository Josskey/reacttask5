import {comments, movies} from "../../data.ts";
import {Genre} from "../../types/genre.ts";
import {useMemo} from "react";

export const useMovies = (rateSort: string = 'desc', genres: Genre[] = [], searchQuery = '') => {
    const filteredMovies = useMemo(() => {
        let filtered = movies.filter(movie => {
            if (genres.length > 0) {
                const movieGenres = movie.genres.map(genre => genre.id);
                return genres.some(selectedGenre => movieGenres.includes(selectedGenre.id));
            }
            return true;
        });

        filtered = searchQuery
            ? filtered.filter(movie => movie.title.toLowerCase().includes(searchQuery.toLowerCase()))
            : filtered;

        return filtered.sort((a, b) => {
            const aRate = comments.filter(comment => comment.movieId === a.id).reduce((rate, comment) => rate + comment.rate, 0);
            const bRate = comments.filter(comment => comment.movieId === b.id).reduce((rate, comment) => rate + comment.rate, 0);
            return rateSort === 'desc' ? bRate - aRate : aRate - bRate;
        });
    }, [rateSort, genres, searchQuery]);

    const findMovieById = (movieId: number) => {
        return movies.find(movie => movie.id === movieId);
    }

    return {
        movies: filteredMovies,
        findMovieById,
    };
}