import {genres} from "../../data.ts";
import {Genre} from "../../types/genre.ts";

export const useGenres = () => {
    const findGenreByNameSlug = (nameSlug: string): Genre | undefined => {
        return genres.find((genre) => genre.nameSlug === nameSlug);
    }

    return {
        genres,
        findGenreByNameSlug,
    };
}