import {useSearchParams} from "react-router-dom";
import {useEffect, useState} from "react";

export const useGenresFromSearchParams = (searchParamKey: string) => {
    const [searchParams] = useSearchParams();
    const [selected, setSelected] = useState<string[]>([]);

    useEffect(() => {
        setSelected((searchParams.get(searchParamKey) ?? '').split(',').filter(genre => genre !== ''));
    }, [searchParams, searchParamKey]);

    return {
        selected,
    };
}
