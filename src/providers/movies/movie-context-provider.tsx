import {ReactNode} from 'react';
import {Movie} from "../../types/movie.ts";
import {MovieContext} from "./movie-context.ts";

export const MovieContextProvider = ({movie, children}: { movie: Movie, children: ReactNode }) => {
    return (
        <MovieContext.Provider value={movie}>
            {children}
        </MovieContext.Provider>
    );
};
