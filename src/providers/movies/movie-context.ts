import {createContext, useContext} from "react";
import {Movie} from "../../types/movie.ts";

export const MovieContext = createContext<Movie>({} as Movie);
export const useMovieContext = () => useContext(MovieContext);
